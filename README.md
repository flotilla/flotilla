# Flotilla

An open-source dispatching engine, built for managing mobile fleets of any
size.

## Contributions & coding standards

Before delving into writing any code, please read through the following page
on the Flotilla wiki:

* [Design goals](https://bitbucket.org/flotilla/flotilla/wiki/Design%20goals)

