/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
	"html/template"
	"net/http"

	"bitbucket.org/flotilla/flotilla/db"
	"github.com/gorilla/context"
	"github.com/gorilla/sessions"
)

type DashboardHandler struct {
	DB           db.DB
	Tpls         *template.Template
	SessionStore sessions.Store
}

func (h DashboardHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.get(w, r)
	}
}

func (h DashboardHandler) get(w http.ResponseWriter, r *http.Request) {
	ctx := struct {
		Username string
	}{
		Username: context.Get(r, CtxUsername).(string),
	}
	if err := h.Tpls.ExecuteTemplate(w, "Dashboard", ctx); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
