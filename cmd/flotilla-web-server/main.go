/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"bitbucket.org/flotilla/flotilla/db"
	"bitbucket.org/flotilla/flotilla/sessions"
	"github.com/gorilla/context"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	gsessions "github.com/gorilla/sessions"
)

const (
	SessionName = "flotilla-session"
)

func main() {
	log.SetPrefix("flotilla-web-server ")
	defer log.Println("Shutting down")

	dburl := os.Getenv("DATABASE_URL")

	log.Println("Connecting to database")
	database, err := db.Open(dburl)
	if err != nil {
		log.Fatalln("error connecting to database:", err)
	}
	log.Println("Connected to database")

	if err := database.Close(); err != nil {
		log.Fatalln("error closing database connection:", err)
	}

	// Create a new session store.
	splenda := []byte(os.Getenv("SECRET_KEY"))
	if len(splenda) == 0 {
		log.Fatalln("environment variable SECRET_KEY not set")
	}
	domain := os.Getenv("DOMAIN")
	if domain == "" {
		domain = "localhost"
	}
	sessionStore, err := sessions.NewSessionStore(dburl, domain, splenda)

	// Load the HTML templates.
	tpldir := os.Getenv("TEMPLATE_DIR")
	tpls, err := template.ParseGlob(filepath.Join(tpldir, "*.html"))
	if err != nil {
		log.Println("error loading templates:", err)
	}

	// Set up the HTTP server.
	staticRoot := os.Getenv("STATIC_ROOT")

	r := mux.NewRouter()
	r.Handle("/login", LoginHandler{DB: database, Tpls: tpls, SessionStore: sessionStore})
	r.PathPrefix("/static").Handler(http.StripPrefix("/static", http.FileServer(http.Dir(staticRoot))))

	dashboard := DashboardHandler{
		DB:           database,
		Tpls:         tpls,
		SessionStore: sessionStore,
	}
	r.Handle("/dashboard", ContextHandler(dashboard, sessionStore))

	r.Handle("/", http.RedirectHandler("/login", http.StatusMovedPermanently))

	port, err := strconv.Atoi(os.Getenv("PORT"))
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("HTTP server listening on :%d", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), handlers.LoggingHandler(os.Stdout, r)); err != nil {
		log.Fatalln("failed to start HTTP server:", err)
	}
}

const (
	CtxSession = iota + 1
	CtxUsername
	CtxUserId
	CtxUserRoles

	SessionUsername = iota + 1
	SessionUserId
	SessionUserRoles
)

func ContextHandler(h http.Handler, sessionStore gsessions.Store) http.Handler {
	return contextHandler{
		Handler:      h,
		SessionStore: sessionStore,
	}
}

type contextHandler struct {
	Handler      http.Handler
	SessionStore gsessions.Store
}

func (h contextHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	session, err := h.SessionStore.Get(r, SessionName)
	if err != nil {
		log.Println("error loading session:", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if username, ok := session.Values[SessionUsername].(string); ok {
		context.Set(r, CtxUsername, username)
	}

	if id, ok := session.Values[SessionUserId].(string); ok {
		context.Set(r, CtxUserId, id)
	}

	if roles, ok := session.Values[SessionUserRoles].([]string); ok {
		context.Set(r, CtxUserRoles, roles)
	}

	h.Handler.ServeHTTP(w, r)
}

type IndexHandler struct {
	SessionStore gsessions.Store
}

func (h IndexHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s, err := h.SessionStore.Get(r, SessionName)
	if err != nil {
		log.Println("error loading session:", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
