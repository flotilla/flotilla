/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package main

import (
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/flotilla/flotilla/db"
	"code.google.com/p/go.crypto/bcrypt"
	"github.com/gorilla/sessions"
)

var (
	DefaultSessionTTL uint64 = 3600
)

type LoginHandler struct {
	DB           db.DB
	Tpls         *template.Template
	SessionStore sessions.Store
}

func (h LoginHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.Tpls.ExecuteTemplate(w, "Login", nil)
	case "POST":
		h.post(w, r)
	}
}

func (h LoginHandler) post(w http.ResponseWriter, r *http.Request) {
	u, p := r.FormValue("username"), r.FormValue("password")
	if u == "" {
		http.Error(w, "no username provided", http.StatusBadRequest)
		return
	}
	if p == "" {
		http.Error(w, "no password provided", http.StatusBadRequest)
		return
	}

	user, err := h.DB.GetUserByUsername(u)
	if err != nil && err == db.ErrNotFound {
		http.Error(w, "login failed", http.StatusBadRequest)
		return
	} else if err != nil {
		log.Println("db query to find user failed:", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Check the user's password.
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(p)); err != nil {
		http.Error(w, "login failed", http.StatusBadRequest)
		return
	}

	session, err := h.SessionStore.New(r, SessionName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	session.Values[SessionUsername] = user.Username
	session.Values[SessionUserId] = user.Id
	session.Values[SessionUserRoles] = user.Roles

	if err = session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/dashboard", http.StatusFound)
}
