FLOTILLA_ROOT_PKG = bitbucket.org/flotilla/flotilla

all: test apps

apps: flotilla-web-server flotilla-dispatch-engine flotilla-communication-daemon

flotilla-web-server: $(wildcard cmd/flotilla-web-server/*.go)
	godep go build ${FLOTILLA_ROOT_PKG}/cmd/$@

flotilla-dispatch-engine: $(wildcard cmd/flotilla-dispatch-engine/*.go)
	godep go build ${FLOTILLA_ROOT_PKG}/cmd/$@

flotilla-communication-daemon: $(wildcard cmd/flotilla-communication-daemon/*.go)
	godep go build ${FLOTILLA_ROOT_PKG}/cmd/$@

save-deps:
	godep save ${FLOTILLA_ROOT_PKG}/...

clean:
	rm -f \
		flotilla-web-server \
		flotilla-dispatch-engine \
		flotilla-communication-daemon

test: $(shell find . -type f -iname "*.go")
	godep go test -v ${FLOTILLA_ROOT_PKG}/...

.PHONY: save-deps clean test apps
