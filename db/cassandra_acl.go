/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package db

import "bitbucket.org/flotilla/flotilla/data"

func (d *cassandraDB) CreateACLRule(r data.ACLRule) error {
	return ErrNotImplemented
}

func (d *cassandraDB) UpdateACLRule(r data.ACLRule) error {
	return ErrNotImplemented
}

func (d *cassandraDB) DeleteACLRule(r data.ACLRule) error {
	return ErrNotImplemented
}

func (d *cassandraDB) GetACLRule(id string) (data.ACLRule, error) {
	return data.ACLRule{}, ErrNotImplemented
}

func (d *cassandraDB) ListACLRules() ([]data.ACLRule, error) {
	return nil, ErrNotImplemented
}
