/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package db

import "testing"

func TestOpen(t *testing.T) {
	// This table holds a database connection URL, and whether or not the
	// test should fail.
	tests := map[string]bool{
		"cql://localhost/flotilla":                            false,
		"cassandra://127.0.0.1/flotilla":                      false,
		"postgres://localhost:5432/flotilla?sslmode=disabled": true,
		"mariadb://localhost:3307/flotilla":                   true,
		"mysql://localhost:3307/flotilla":                     true,
		"mongodb://localhost:27017/flotilla":                  true,
	}

	for u, fail := range tests {
		_, err := Open(u)
		if (err == nil) == fail {
			t.Error(err)
		}
	}
}
