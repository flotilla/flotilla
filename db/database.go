/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package db

import (
	"errors"

	"bitbucket.org/flotilla/flotilla/data"
)

var (
	ErrNotImplemented = errors.New("not implemented")
	ErrNotFound       = errors.New("not found")
)

type FleetStore interface {
	CreateFleet(f data.Fleet) error
	UpdateFleet(f data.Fleet) error
	DeleteFleet(f data.Fleet) error
	GetFleet(id string) (data.Fleet, error)
	ListFleets() ([]data.Fleet, error)
}

type ACLStore interface {
	CreateACLRule(r data.ACLRule) error
	UpdateACLRule(r data.ACLRule) error
	DeleteACLRule(r data.ACLRule) error
	GetACLRule(id string) (data.ACLRule, error)
	ListACLRules() ([]data.ACLRule, error)
}

type UserStore interface {
	CreateUser(u data.User) error
	UpdateUser(u data.User) error
	DeleteUser(u data.User) error
	GetUser(id string) (data.User, error)
	GetUserByUsername(u string) (data.User, error)
	ListUsers() ([]data.User, error)
}

type RoleStore interface {
	CreateRole(r data.Role) error
	UpdateRole(r data.Role) error
	DeleteRole(r data.Role) error
	ListRoles() ([]data.Role, error)
}

type DB interface {
	FleetStore
	ACLStore
	UserStore
	RoleStore

	Close() error
}
