/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package db

import (
	"fmt"
	"net/url"
)

type ErrNotSupported struct {
	DBType string
}

func (e *ErrNotSupported) Error() string {
	return fmt.Sprintf("unsupported database type: %s", e.DBType)
}

func Open(s string) (DB, error) {
	u, err := url.Parse(s)
	if err != nil {
		return nil, err
	}

	switch scheme := u.Scheme; scheme {
	case "cql", "cassandra":
		return openCassandraDB(u)
	case "postgres":
		return nil, &ErrNotSupported{DBType: scheme}
	case "mariadb":
		return nil, &ErrNotSupported{DBType: scheme}
	case "mysql":
		return nil, &ErrNotSupported{DBType: scheme}
	case "mongodb":
		return nil, &ErrNotSupported{DBType: scheme}
	default:
		return nil, &ErrNotSupported{DBType: scheme}
	}
}
