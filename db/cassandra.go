/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package db

import (
	"errors"
	"net/url"
	"strings"

	"github.com/gocql/gocql"
)

func openCassandraDB(u *url.URL) (DB, error) {
	hosts := strings.Split(u.Host, ",")
	cluster := gocql.NewCluster(hosts...)

	ks := strings.Split(u.Path, "/")[1]
	if ks == "" {
		return nil, errors.New("no keyspace name provided in URL")
	}

	cluster.Keyspace = ks

	switch nhosts := len(hosts); nhosts {
	case 1:
		cluster.Consistency = gocql.One
	default:
		cluster.Consistency = gocql.Quorum
	}

	session, err := cluster.CreateSession()
	d := &cassandraDB{
		Cluster: cluster,
		Session: session,
	}

	return d, err
}

type cassandraDB struct {
	Cluster *gocql.ClusterConfig
	Session *gocql.Session
}

func (d *cassandraDB) withSession(fn func(s *gocql.Session) error) error {
	dbs, err := d.Cluster.CreateSession()
	if err != nil {
		return err
	}
	defer dbs.Close()

	return fn(dbs)
}

func (d *cassandraDB) Close() error {
	d.Session.Close()
	return nil
}
