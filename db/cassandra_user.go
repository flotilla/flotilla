/*
   Copyright 2014 Flotilla developers

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package db

import (
	"bitbucket.org/flotilla/flotilla/data"
	"github.com/gocql/gocql"
)

func (d *cassandraDB) CreateUser(u data.User) error {
	return ErrNotImplemented
}

func (d *cassandraDB) UpdateUser(u data.User) error {
	return ErrNotImplemented
}

func (d *cassandraDB) DeleteUser(u data.User) error {
	return ErrNotImplemented
}

func (d *cassandraDB) GetUser(id string) (data.User, error) {
	return data.User{}, ErrNotImplemented
}

func (d *cassandraDB) GetUserByUsername(username string) (data.User, error) {
	session, err := d.Cluster.CreateSession()
	if err != nil {
		return data.User{}, err
	}
	defer session.Close()

	var id gocql.UUID
	var enabled bool
	var password string
	var roles []string

	q := `SELECT id, enabled, password, roles FROM users WHERE username = ? LIMIT 1`
	err = session.Query(q, username).Consistency(gocql.One).Scan(&id, &enabled, &password, &roles)
	if err != nil && err == gocql.ErrNotFound {
		return data.User{}, ErrNotFound
	} else if err != nil {
		return data.User{}, err
	}

	u := data.User{
		Id:       id.String(),
		Username: username,
		Enabled:  enabled,
		Password: password,
		Roles:    roles,
	}

	return u, nil
}

func (d *cassandraDB) ListUsers() ([]data.User, error) {
	return nil, ErrNotImplemented
}
