package sessions

import (
	"errors"
	"fmt"
	"net/url"
	"strings"

	"github.com/gocql/gocql"
	gsessions "github.com/gorilla/sessions"
	"github.com/nesv/cassandrastore"
)

func NewSessionStore(dbUrl, domain string, keypairs ...[]byte) (gsessions.Store, error) {
	u, err := url.Parse(dbUrl)
	if err != nil {
		return nil, err
	}

	switch u.Scheme {
	case "cql", "cassandra":
		hosts := strings.Split(u.Host, ",")
		cluster := gocql.NewCluster(hosts...)

		keyspace := strings.Split(u.Path, "/")[1]
		if keyspace == "" {
			return nil, errors.New("no keyspace provided in URL")
		}
		cluster.Keyspace = keyspace

		switch u.Query().Get("consistency") {
		case "any":
			cluster.Consistency = gocql.Any
		case "one":
			cluster.Consistency = gocql.One
		case "two":
			cluster.Consistency = gocql.Two
		case "three":
			cluster.Consistency = gocql.Three
		case "quorum":
			cluster.Consistency = gocql.Quorum
		case "all":
			cluster.Consistency = gocql.All
		case "localquorum":
			cluster.Consistency = gocql.LocalQuorum
		case "eachquorum":
			cluster.Consistency = gocql.EachQuorum
		case "serial":
			cluster.Consistency = gocql.Serial
		case "localserial":
			cluster.Consistency = gocql.LocalSerial
		case "localone":
			cluster.Consistency = gocql.LocalOne
		}

		store, err := cassandrastore.NewCassandraStore(cluster, "sessions", keypairs...)
		store.Options.Domain = domain

		return store, err
	}

	return nil, fmt.Errorf("unknown session store scheme %s", u.Scheme)
}
