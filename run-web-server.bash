#!/usr/bin/env bash

set -e

PORT=8000
DATABASE_URL=cql://127.0.0.1/flotilla
STATIC_ROOT=$(pwd)/static
TEMPLATE_DIR=$(pwd)/templates
SECRET_KEY="some-really-secret-dev-key"
export PORT DATABASE_URL STATIC_ROOT TEMPLATE_DIR SECRET_KEY

cleanup() {
	make clean
}
trap cleanup EXIT

make flotilla-web-server
./flotilla-web-server
